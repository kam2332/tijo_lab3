package kotlinsrc

import kotlin.properties.Delegates

open class Point2D() {

    internal var x: Double = 0.0
    internal var y: Double = 0.0

    constructor(x: Double, y: Double) : this() {
        this.x = x
        this.y = y
    }

    override fun toString(): String {
        return "x = $x y = $y"
    }
}
