package kotlinsrc

import kotlin.properties.Delegates

class MaterialPoint2D : Point2D {

    internal var mass: Double = 0.0

    constructor(x: Double, y: Double, mass: Double) : this() {
        this.x = x
        this.y = y
        this.mass = mass
    }

    constructor()

    override fun toString(): String {
        return "x = $x y = $y mass = $mass"
    }
}
