package kotlinsrc

object Calculations {

    fun positionGeometricCenter(points: Array<Point2D>) : Point2D {
        var sumX = 0.0
        var sumY = 0.0

        for(point in points) {
            sumX += point.x
            sumY += point.y
        }
        sumX /= points.size
        sumY /= points.size

        return Point2D(sumX, sumY)
    }

    fun positionCenterOfMass(points: Array<MaterialPoint2D>) : Point2D {
        var sumX = 0.0
        var sumY = 0.0
        var sumOfMass = 0.0

        for (point in points) {
            sumX += point.mass * point.x
            sumY += point.mass * point.y
            sumOfMass += point.mass
        }

        sumX /= sumOfMass
        sumY /= sumOfMass

        return MaterialPoint2D(sumX, sumY, sumOfMass)
    }
}
