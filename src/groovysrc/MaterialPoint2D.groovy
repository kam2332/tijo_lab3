package groovysrc

class MaterialPoint2D extends Point2D {

    protected double mass;

    MaterialPoint2D(double x, double y, double mass) {
        super(x, y);
        this.mass = mass;
    }

    @Override
    String toString() {
        return "x = " + x +
                " y = " + y +
                " mass = " + mass;
    }
}
