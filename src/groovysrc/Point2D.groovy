package groovysrc

class Point2D {

    protected double x;
    protected double y;

    Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    String toString() {
        return "x = " + x +
                " y = " + y;
    }
}
