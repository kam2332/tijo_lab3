package javasrc;

public class Calculations {

    public static Point2D positionGeometricCenter(Point2D[] point) {
        double sumX = 0;
        double sumY = 0;
        for (Point2D point2D : point) {
            sumX += point2D.x;
            sumY += point2D.y;
        }
        sumX /= point.length;
        sumY /= point.length;

        return new Point2D(sumX, sumY);
    }

    public static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoint) {
        double sumX = 0;
        double sumY = 0;
        double sumOfMass = 0;
        for (MaterialPoint2D point2D : materialPoint) {
            sumX += point2D.mass * point2D.x;
            sumY += point2D.mass * point2D.y;
            sumOfMass += point2D.mass;
        }
        sumX /= sumOfMass;
        sumY /= sumOfMass;

        return new MaterialPoint2D(sumX, sumY, sumOfMass);
    }
}
